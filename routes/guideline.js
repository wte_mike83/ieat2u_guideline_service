var express = require('express');
var router = express.Router();
const passport = require('passport');
const GuidelineController = require('./../controller/GuidelineController')


DeviceAuth = function (req, res, next) {

    require('./../middleware/passport').MerchantAuth(passport);

    passport.authenticate('jwt', function (err, user, info) {

        if (!user) {
            if (info)
                return ReE(res, info.message, 401);

            return ReE(res, err, 401);
        }
        req.merchant = user;
        return next();

    })(req, res, next);

};



MerchantAuthMiddleware = function(req, res, next) {
    require('./../middleware/passport').merchantAuth(passport);
    passport.authenticate('jwt', function(err, user, info) {
 
        if(!user)
        {
            if(info)
                return ReE(res,info.message,401);
 
            return ReE(res,err,401);
        }
        req.user = user;
        return next();
 
    })(req, res, next);
 
 };


 AdminAuthMiddleware = function(req, res, next) {

    require('./../middleware/passport').adminAuth(passport);

    passport.authenticate('jwt', function(err, user, info) {
        
        if(!user)
        {   
            if(info)
                return ReE(res,info.message,401);
                
            return ReE(res,err,401);
        }
        req.user = user;
        return next();
  
    })(req, res, next);
  
};


 
 permissionAccess  = function(req,res,next){
 
    let merchant = req.user;
 
    let permission = JSON.parse(merchant.permission);
 
    req.checkParams({
        'rid':{
            notEmpty: true,
            errorMessage: 'Restaurant ID is required',
            isInt:{
                errorMessage: 'Invalid Restaurant ID',
            }
        }
    }); //validation here
 
 
    var errors = req.validationErrors(); // get validation error
 
    if (errors)
    {
        return ReE(res, errors,400); // return error
    }
 
    let restaurant_id = req.params.rid;
 
    if(permission[restaurant_id] /*&& permission[restaurant_id].product_availability*/)
    {
        return next();
 
    }else
        return ReE(res,'Unauthorized access',401);
 
 };


/**
 * @api {post} admin/guideline/uploadfile Upload File
 * @apiName Admin UploadFile
 * @apiGroup Admin


 * 
 *
 * @apiHeader {String} Content-Type multipart/form-data
 * 
 * @apiParam {String} title     [Optional] *ignore will set title as filename
 * @apiParam {String} tag       [Optional]
 * @apiParam {File} file        insert file
 *
 * @apiSuccess {boolean}                                    success true or false
 * @apiSuccess {String} message                             Return Message.
 * 
 *                    

 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
    {
      "data": {
        "tutorial_id": 46,
        "title": "test return value",
        "type": "application/pdf",
        "updatedAt": "2018-11-30T09:11:11.639Z",
        "createdAt": "2018-11-30T09:11:11.639Z"
    },
    "success": true,
    "message": "File Uploaded Successfully"
    }
 *


 * @apiError {boolean} success True or False
 * @apiError {String} error Return Error Message.
 * 

 * @apiErrorExample Wrong File Format
 *     HTTP/1.1 400 wrong file format
 *     {
 *        "success": false,
 *         "message": "Only pdf or video file are allowed"
 *     } 
 * 
 * @apiErrorExample Wrong File Field
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "success": true,
 *           "message": "invalid field"
 *     }  
 * 
 */

router.post('/admin/guideline/uploadfile',AdminAuthMiddleware,GuidelineController.UploadFile);
//router.post('/admin/guideline/uploadfile',GuidelineController.UploadFile);




/**
 * @api {post} admin/guideline/modifylist Edit Guideline List
 * @apiName Admin Modifylist
 * @apiGroup Admin


 * 
 *
 * @apiHeader {String} Content-Type multipart/form-data
 * @apiParam {String} id     title [require] 
 * @apiParam {String} title     title [Optional] 
 * @apiParam {String} tag       tag [Optional]
 * @apiParam {File} file        insert file, *file type must same with previous file
 *
 * @apiSuccess {boolean}                                    success true or false
 * @apiSuccess {String} message                             Return Message.
 * 
 *                    

 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
    {
        "success": true,
        "message": "Successfully edit"
        }
 *


 * @apiError {boolean} success True or False
 * @apiError {String} error Return Error Message.
 * 

 * @apiErrorExample Wrong Tutorial ID
 *     HTTP/1.1 400 wrong tutorial id
 *   {
    "success": false,
    "message": [
        {
            "location": "body",
            "param": "id",
            "msg": "Invalid value"
        },
        {
            "location": "body",
            "param": "id",
            "msg": "Invalid Tutorial Id"
        }
    ]
}
 * 
 * @apiErrorExample Wrong File Type
 *     HTTP/1.1 400 Bad Request
 *    {
    "success": false,
    "message": "wrong file type with previous file, please upload again "
}
 * 
 */

//router.post('/admin/guideline/modifylist',GuidelineController.modifylist);
router.post('/admin/guideline/modifylist',AdminAuthMiddleware,GuidelineController.modifylist);

/**
 * @api {post} admin/guideline/deletefile Delete File
 * @apiName Admin deletefile
 * @apiGroup Admin


 * 
 *
 * @apiHeader {String} Content-Type application/x-www-form-urlencoded
 * 
 * @apiParam {String} id            tutorial id
 *
 * @apiSuccess {boolean}                                    success true or false
 * @apiSuccess {String} message                             Return Message.
 * 
 *                    
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
    {
        
    "success": true,
    "message": "Successfully get data"
    }
 *


 * @apiError {boolean} success True or False
 * @apiError {String} error Return Error Message.
 * 

 * @apiErrorExample record not found
 *     HTTP/1.1 400 record not found
 {
    "success": false,
    "message": "record not found"
}
 * 

 */

router.post('/admin/guideline/deletefile',AdminAuthMiddleware,GuidelineController.DeleteFile);

/**
 * @api {post} admin/guideline/list get Guideline List
 * @apiName Admin GuidelineList
 * @apiGroup Admin

 * 
 *
 * @apiHeader {String} Content-Type application/x-www-form-urlencoded
 * 
 *
 * @apiSuccess {boolean}                                    success true or false
 * @apiSuccess {String} message                             Return Message.
 * 
 * @apiParam {String} search                                 search by title[Optional]           
 * @apiParam {String} type                                    'mp4' or 'pdf'[Optional]    
 * 
  * @apiSuccess {String} data/path                           path
 * @apiSuccess {String} data/menu/tutorial_id               tutorial_id
 * @apiSuccess {String} data/menu/title                      title
 * @apiSuccess {String} data/menu/type                          type
 * @apiSuccess {String} data/menu/tag                   tag

 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
    {
        
    "data": [
        {
            "path": "http://129.126.133.187:4001//images/guideline/video/givap1543476104233.mp4",
            "tutorial_id": 28,
            "title": "testing 124",
            "type": "video/mp4",
            "tag": null
        },
        {
            "path": "http://129.126.133.187:4001//images/guideline/pdf/wdee51543476199417.pdf",
            "tutorial_id": 33,
            "title": "Merchant Interface Specification for FPX v4",
            "type": "application/pdf",
            "tag": null
        },
        {
            "path": "http://129.126.133.187:4001//images/guideline/pdf/jii6v1543476615923.pdf",
            "tutorial_id": 38,
            "title": "this is testing",
            "type": "application/pdf",
            "tag": null
        }
     
    ],
    "success": true,
    "message": "Successfully get data"
    }
 *


 * @apiError {boolean} success True or False
 * @apiError {String} error Return Error Message.
 * 
* @apiErrorExample wrong file format
 *     HTTP/1.1 400 wrong file format
 *     {
 *        "success": false,
 *         "message": "Only pdf or video file are allowed"
 *     } 
 * 
 */


router.post('/admin/guideline/list',AdminAuthMiddleware, GuidelineController.getGuidelineList);


/**
 * @api {post} merchant/guideline/list get Guideline List
 * @apiName Merchant GuidelineList
 * @apiGroup Merchant

 * 
 *
 * @apiHeader {String} Content-Type application/x-www-form-urlencoded
 * 
 *
 * @apiSuccess {boolean}                                    success true or false
 * @apiSuccess {String} message                             Return Message.
 * 
 * @apiParam {String} search                                 search by title[Optional]           
 * 
 * 
  * @apiSuccess {String} data/path                           path
 * @apiSuccess {String} data/menu/tutorial_id               tutorial_id
 * @apiSuccess {String} data/menu/title                      title
 * @apiSuccess {String} data/menu/type                          type
 * @apiSuccess {String} data/menu/tag                   tag

 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
    {
        
    "data": [
        {
            "path": "http://129.126.133.187:4001//images/guideline/video/givap1543476104233.mp4",
            "tutorial_id": 28,
            "title": "testing 124",
            "type": "video/mp4",
            "tag": null
        },
        {
            "path": "http://129.126.133.187:4001//images/guideline/pdf/wdee51543476199417.pdf",
            "tutorial_id": 33,
            "title": "Merchant Interface Specification for FPX v4",
            "type": "application/pdf",
            "tag": null
        },
        {
            "path": "http://129.126.133.187:4001//images/guideline/pdf/jii6v1543476615923.pdf",
            "tutorial_id": 38,
            "title": "this is testing",
            "type": "application/pdf",
            "tag": null
        }
     
    ],
    "success": true,
    "message": "Successfully get data"
    }
 *


 * @apiError {boolean} success True or False
 * @apiError {String} error Return Error Message.
 * 

 * 
 */


router.post('/merchant/guideline/list',MerchantAuthMiddleware, GuidelineController.getGuidelineList);

// router.post('/admin/guideline/list', GuidelineController.getGuidelineList);
// router.post('/merchant/guideline/list', GuidelineController.getGuidelineList);


module.exports = router;