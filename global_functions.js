to = function(promise) {//global function that will help use handle promise rejections, this article talks about it http://blog.grossman.io/how-to-write-async-await-without-try-catch-blocks-in-javascript/
    return promise
    .then(data => {
        return [null, data];
    }).catch(err =>
        [pe(err)]
    );
}

pe = require('parse-error');//parses error so you can read error message and handle them accordingly

TE = function(err_message, log){ // TE stands for Throw Error
    if(log === true){
        console.error(err_message);
    }

    throw new Error(err_message);
}

ReE = function(res, err, code){ // Error Web Response
    if(typeof err == 'object' && typeof err.message != 'undefined'){
        err = err.message;
    }

    if(typeof code !== 'undefined') res.statusCode = code;

    return res.json({success:false, message: err});
}

ReS = function(res, msg, data, code){ // Success Web Response
    let send_data = {success:true, message: msg};

    if(typeof data == 'object'){
        send_data = Object.assign(data, send_data);//merge the objects
    }

    if(typeof code !== 'undefined') res.statusCode = code;

    return res.json(send_data)
};

request = require('request');

// SMS = function(mobile,msg)
// {
//     var headers = {
//         'User-Agent':       'Super Agent/0.0.1',
//         'Content-Type':     'application/x-www-form-urlencoded'
//     }

//     var urloption = {
//             api_key : CONFIG.trioApi,
//             action  : 'send',
//             msg     : msg,
//             to      : mobile,
//             sender_id : 'irider',
//             content_type : '1',
//             mode    :'shortcode'
//     };

//     var options = {
//         url: 'http://cloudsms.trio-mobile.com/index.php/api/bulk_mt',
//         method: 'GET',
//         qs: urloption,
//         headers: headers,
//     }

//     request.get(options,
//         function (error, response, body) {
//             if (!error && response.statusCode == 200) {
//                 console.log(body)
//             }
//         }
//     );
    
//     return true;
// }

randomStr = function(m){

	var m = m || 9; s = '', r = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
	for (var i=0; i < m; i++) { s += r.charAt(Math.floor(Math.random()*r.length)); }
    return s;
    
};

generateRandomKey = function(length) {

    let start = 2;
    let stop = parseInt(length) + start;

    return Math.random().toString(36).substring(start, stop);
  
}

Keyfilter = function (body,filterkey){
    
    const filtered = Object.keys(body)
      .filter(key => filterkey.includes(key))
      .reduce((obj, key) => {
        obj[key] = body[key];
        return obj;
      }, {});

    return filtered;
}

checkDirectorySync = function(directory){
    const fs = require('fs');
    return new Promise((resolve, reject) => {
        fs.stat(directory, function(err) {
            if(err)
            {
                 //Check if error defined and the error code is "not exists"
                if (err.code === 'ENOENT') {
                    fs.mkdir(directory, (error) => {
                        if (error) {
                            if(err.code === 'ENOENT')
                                reject(new Error('Upload Directory Not Found!'));
                            else
                                reject(error);
                        } else {
                            resolve(directory);
                        }
                    });
                }else {
                    //just in case there was a different error:            
                    reject(err);
                }
            } else {
                resolve(directory);
            }
        });
    });
}

//This is here to handle all the uncaught promise rejections
process.on('unhandledRejection', error => {
    console.error('Uncaught Error', pe(error));
});



