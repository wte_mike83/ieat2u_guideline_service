const moment = require('moment');
'use strict';
module.exports = (sequelize, DataTypes) => {

  var ConsumerOrder = sequelize.define('ConsumerOrder', {

    id: {type:DataTypes.INTEGER ,primaryKey: true,autoIncrement: true} ,
    tax_invoice:DataTypes.STRING,
    consumer_id: DataTypes.INTEGER.UNSIGNED,
    fullname:DataTypes.STRING,
    contact:DataTypes.STRING,
    restaurant_id:DataTypes.INTEGER.UNSIGNED,
    restaurant_name:DataTypes.STRING,
    restaurant_address:DataTypes.STRING,
    restaurant_latitude:DataTypes.STRING,
    restaurant_longitude:DataTypes.STRING,
    restaurant_contact:DataTypes.STRING,
    order_type:DataTypes.STRING,
    order_date:'TIMESTAMP',
    estimated_datetime:'TIMESTAMP',
    place_order_date:'TIMESTAMP',
    menu:DataTypes.STRING,
    total:DataTypes.DECIMAL,
    payment:DataTypes.STRING,
    voucher_amount:DataTypes.DECIMAL,
    remark:DataTypes.STRING,
    status:DataTypes.INTEGER,
    payment_time:'TIMESTAMP',
    cancel_time:'TIMESTAMP',
    reason:DataTypes.STRING,
    dinein_session:DataTypes.STRING,
    secret_code:DataTypes.INTEGER,
    restaurant_discount:DataTypes.STRING,
    sys_remark:DataTypes.TEXT,
    extra:DataTypes.TEXT,
    refer_order_id:DataTypes.STRING,
  }, {
    freezeTableName: true,
    tableName: 'consumer_order',
    timestamps: false, 
    getterMethods: { 
      status_text:  function() {
        
        var delivery_stat = {
            '0' :'PAYMENT PROCESSING',
            '1' :"AWAITING CONFIRMATION",
            '2' :"CONFIRMED",
            '3' :"DISH READY",
            '4' :"AWAITING DELIVERY",
            '5' :"IN-DELIVERY",
            '6' :"COMPLETED",
            '7' :"CANCELLED by Merchant",
            '8' :"CANCELLED by Client",
            '9' :"CANCELLED by System"        
        };
    
        var pickup_stat = {
            '0' :'PAYMENT PROCESSING',
            '1' :"AWAITING CONFIRMATION",
            '2' :"CONFIRMED",
            '3' :"DISH READY",
            '6' :"COMPLETED",
            '7' :"CANCELLED by Merchant",
            '8' :"CANCELLED by Client",
            '9' :"CANCELLED by System" 
        };
    
        var dinein_stat = {
          '1' :"AWAITING CONFIRMATION",
          '2' :"CONFIRMED",
          '6' :"COMPLETED",
          '7' :"CANCELLED by Merchant",
          '8' :"CANCELLED by Client",
          '9' :"CANCELLED by System" 

        };

        if(this.getDataValue('order_type')=='dine_in')
            return dinein_stat[this.status];
        else if(this.getDataValue('order_type')=='delivery')
            return delivery_stat[this.status]
        else
            return pickup_stat[this.status]

      },  menu: function () {
        if(this.getDataValue('menu'))
            return JSON.parse(this.getDataValue('menu'));
       
    else if(this.getDataValue('menu') === undefined)
        return ;
    else 
      return null;

      },

      order_date: function () {
        if(this.getDataValue('order_date'))
            return moment(this.getDataValue('order_date'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
        else if(this.getDataValue('order_date') === undefined)
            return ;
        else 
          return null;
      },
      confirm_time: function () {
        if(this.getDataValue('confirm_time'))
            return moment(this.getDataValue('confirm_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
        else if(this.getDataValue('confirm_time') === undefined)
            return ;
        else 
          return null;
      },
      cook_time: function () {
        if(this.getDataValue('cook_time'))
            return moment(this.getDataValue('cook_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
        else if(this.getDataValue('cook_time') === undefined)
            return ;
        else 
          return null;
      },
      payment_time: function () {
        if(this.getDataValue('payment_time')!=null)
            return moment(this.getDataValue('payment_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
        else if(this.getDataValue('payment_time') === undefined)
            return ;
        else 
          return null;
      },
      dish_ready_time: function () {
        if(this.getDataValue('dish_ready_time'))
            return moment(this.getDataValue('dish_ready_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
        else if(this.getDataValue('dish_ready_time') === undefined)
            return ;
        else 
          return null;
      },
      in_delivery_time: function () {
        if(this.getDataValue('in_delivery_time'))
            return moment(this.getDataValue('in_delivery_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
        else if(this.getDataValue('in_delivery_time') === undefined)
            return ;
        else 
          return null;
      },
      delivered_time: function () {
        if(this.getDataValue('delivered_time'))
            return moment(this.getDataValue('delivered_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
        else if(this.getDataValue('delivered_time') === undefined)
            return ;
        else 
          return null;
      },
      complete_time: function () {
        if(this.getDataValue('complete_time'))
            return moment(this.getDataValue('complete_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
        else if(this.getDataValue('complete_time') === undefined)
            return ;
        else 
          return null;
      },
      reject_time: function () {
        if(this.getDataValue('reject_time'))
            return moment(this.getDataValue('reject_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
        else if(this.getDataValue('reject_time') === undefined)
            return ;
        else 
          return null;
      },
      estimated_datetime: function () {
        if(this.getDataValue('estimated_datetime'))
            return moment(this.getDataValue('estimated_datetime'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
        else if(this.getDataValue('estimated_datetime') === undefined)
            return ;
        else 
          return null;
      },
        extra: function () {
          
          if(this.getDataValue('extra'))
          
              return JSON.parse(this.getDataValue('extra'));
          
          else if(this.getDataValue('extra') === undefined)
              return ;
          else 
              return null;
                
        }
  },

  });

  ConsumerOrder.associate = function(models) {

    // this.restaurant_id = this.belongsTo(models.restaurant, {foreignKey: 'restaurant_id'});

    // ConsumerOrder.belongsTo(models.Consumer, {
    //   foreignKey: 'consumer_id'
    // });

    // ConsumerOrder.hasMany(models.ConsumerVoucher, {
    //   foreignKey: 'order_id',
    // });

    // ConsumerOrder.hasMany(models.ConsumerOrderTime, {
    //   foreignKey: 'order_id',
    //   targetKey:'id'
    // });

    ConsumerOrder.hasOne(models.ConsumerOrderDelivery, {
        foreignKey: 'order_id',
      });


    // this.dinein_session = this.belongsTo(models.ConsumerTable, {foreignKey: 'dinein_session',targetKey:'session_id'});
    
    
  };

  return ConsumerOrder;
};