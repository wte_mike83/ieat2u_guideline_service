// module.exports = {
//  mongoURI: 'mongodb://yc:123456789@ds225010.mlab.com:25010/ieat2u',// connect to dev
//   // mongoURI: 'mongodb://ieat2u_mongoUser88173:WTHqACE4ru77St@127.0.0.1:27017/ieat2u',   //connect to server
//   secretOrKey: 'secret'
// };

require('dotenv').config();//instatiate environment variables

module.exports = {
    mongoURI: 'mongodb://'+process.env.MONGO_USER+':'+process.env.MONGO_PASSWORD+'@'+process.env.MONGO_HOST+'/'+process.env.MONGO_NAME,// connect to dev
     
     secretOrKey: 'secret'
   };