var winston = require('winston');
var moment = require('moment');

var options = {
   fcmNotification:{
      level: 'debug',
      filename: `./logs/kotReminder/${moment().format('Y-MM-DD')}.log`,
      handleExceptions: true,
      json: true,
      maxsize: 5242880, // 5MB
      maxFiles: 5,
      colorize: false,
      timestamp:false
    }
  };

  

//   var logger = new winston.Logger({
//     transports: [
//       new winston.transports.File(options.file),
//       new winston.transports.Console(options.console)
//     ],
//     exitOnError: false, // do not exit on handled exceptions
//   });


//   logger.stream = {
//     write: function(message, encoding) {
//       // use the 'info' log level so the output will be picked up by both transports (file and console)
//       logger.info(message);
//     },
//   };
  
// module.exports.logger = logger;

  var transLogger = new winston.Logger({
    transports: [
      new winston.transports.File(options.snp),
    ],
    exitOnError: false, // do not exit on handled exceptions
  });

module.exports.transLogger = transLogger;

  