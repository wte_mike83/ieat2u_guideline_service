require('./config/config'); 
require('./global_functions');  


var createError = require('http-errors');
var express = require('express');
var logger = require('morgan');
var validator = require('express-validator');



const passport  = require('passport');


var guideLineRouter = require('./routes/guideline');


var bodyParser = require('body-parser')
var helmet = require('helmet');
const { sanitizeBody } = require('express-validator/filter');


var app = express();

app.use(helmet());
app.use(helmet.frameguard());


var DEVICE = 0;

app.use(logger('dev'));
app.use(bodyParser.json({defaultCharset: 'utf-8',limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: false,limit: '50mb'}));

//Passport
app.use(passport.initialize());

const models = require("./models");
models.sequelize.authenticate().then(() => {
    console.log('Connected to SQL database:', CONFIG.db_name);
})
.catch(err => {
    console.error('Unable to connect to SQL database:', CONFIG.db_name, err);
});

app.use(sanitizeBody(['*']).trim())
app.use(validator());

app.use(validator({
    customValidators: {
        checkBoolean: function (value){
            if(value === undefined || parseInt(value) === 0 || parseInt(value) === 1 )
              return true;
            return false;
          
        },
        isEmpty: function (value, input) {
            if (!value.trim()) {
                return false;
            }
            return true;
              
        }
    }
  }));





app.use(express.static('public'))
//app.use('/',AuthMiddleware,express.static('public'))


app.use('/', guideLineRouter);

app.get('/test', function(req,res){
  return ReS(res,'test');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
  
    // render the error page
    res.status(err.status || 500).json({
      message: err.message,
      error: err
    });
});

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

var port = normalizePort(process.env.PORT) || '3010';
app.listen(port, () => console.log(`Server listening on port ${port}`));
  

module.exports = app;
