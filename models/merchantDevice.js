'use strict';
const jwt    = require('jsonwebtoken');
module.exports = (sequelize, DataTypes) => {

  var MerchantDevice = sequelize.define('MerchantDevice', {

    device_id:{type:DataTypes.INTEGER ,primaryKey: true,autoIncrement: true} ,
    app_token:DataTypes.STRING,
    uuid:DataTypes.STRING,
    merchant_id:DataTypes.INTEGER,

  }, {
    freezeTableName: true,
    tableName: 'merchant_device',
    timestamps: false

  });

  MerchantDevice.associate = function(models) {
    
    // associations can be defined here
    //this.device_id = this.hasMany(models.ConsumerCart, {foreignKey: 'device_id'});
  };


  MerchantDevice.prototype.toWeb = function (pw) {
    let json = this.toJSON();
    delete json['password'];

    return json;
  };

  MerchantDevice.prototype.getJWT = function () {
    let expiration_time = parseInt(CONFIG.jwt_expiration);
    return "Bearer "+jwt.sign({device_id:this.device_id}, CONFIG.jwt_encryption_merchant, {expiresIn: expiration_time});
  };



  return MerchantDevice;
};