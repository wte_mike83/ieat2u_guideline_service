'use strict';
const jwt    = require('jsonwebtoken');
module.exports = (sequelize, DataTypes) => {

  var MerchantPermission = sequelize.define('MerchantPermission', {

    merchant_id:{type:DataTypes.INTEGER.UNSIGNED ,primaryKey: true,autoIncrement: true} ,
    restaurant_id   : DataTypes.INTEGER.UNSIGNED,
    permission:DataTypes.STRING,

  }, {
    freezeTableName: true,
    tableName: 'merchant_permission',
    timestamps: false

  });

  MerchantPermission.associate = function(models) {
    
    // associations can be defined here
    this.restaurant_id = this.hasMany(models.restaurant, {foreignKey: 'restaurant_id'});
  };


  return MerchantPermission;
};