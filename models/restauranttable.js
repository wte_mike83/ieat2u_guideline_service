'use strict';

module.exports = (sequelize, DataTypes) => {

  var RestaurantTable = sequelize.define('RestaurantTable', {

    table_id:{type:DataTypes.INTEGER.UNSIGNED ,primaryKey: true,autoIncrement: true} ,
    table_uuid:DataTypes.STRING,
    restaurant_id   : DataTypes.INTEGER.UNSIGNED,
    table_name:DataTypes.STRING,
    table_pax:DataTypes.INTEGER,
    floor_level:DataTypes.INTEGER,
    position_x:DataTypes.DECIMAL,
    position_y:DataTypes.DECIMAL,
    active:DataTypes.INTEGER,
    display_floor:DataTypes.STRING,

  }, {
    freezeTableName: true,
    tableName: 'restaurant_table',

  });

  RestaurantTable.associate = function(models) {
    
    // associations can be defined here
    // this.restaurant_id = this.belongsTo(models.restaurant, {foreignKey: 'restaurant_id'});
    // this.table_id = this.hasMany(models.ConsumerTable, {foreignKey: 'table_id'});
    this.table_id = this.hasMany(models.RestaurantReservation, {foreignKey: 'table_id'});
  };


  RestaurantTable.prototype.toWeb = function (pw) {
    return this.toJSON();
  };

  return RestaurantTable;
};