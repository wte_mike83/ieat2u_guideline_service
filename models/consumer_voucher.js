'use strict';
module.exports = (sequelize, DataTypes) => {
  var ConsumerVoucher = sequelize.define('ConsumerVoucher', {
    voucher_id: {type:DataTypes.INTEGER.UNSIGNED ,primaryKey: true,autoIncrement: true},
    package_id: DataTypes.INTEGER.UNSIGNED,
    consumer_id: DataTypes.INTEGER.UNSIGNED,
    trans_id: DataTypes.STRING,
    uuid: DataTypes.STRING,
    status: DataTypes.STRING,
    order_id: DataTypes.INTEGER.UNSIGNED,
    use_time:'TIMESTAMP',
    payment_time:'TIMESTAMP',

  }, {
    freezeTableName: true,
    tableName: 'consumer_voucher',
    getterMethods: {

      status_text: function() {

        if(this.getDataValue('status'))
        {

          var status = {
              '1' :"Available",
              '2' :"Redeemed",
              '3' :"Expired",
              '4' :"Refund"
          }
    
          return status[this.getDataValue('status')];
        }

        return null;
      }
    }
  });
  
  ConsumerVoucher.associate = function(models) {
    // associations can be defined here

    this.package_id = this.belongsTo(models.SpecialPackage, {foreignKey: 'package_id'});
    this.consumer_id = this.belongsTo(models.Consumer, {foreignKey: 'consumer_id'});
    this.use_restaurant = this.hasMany(models.restaurant, {foreignKey: 'restaurant_id'});
    this.buy_from = this.hasMany(models.restaurant, {foreignKey: 'restaurant_id'});
    // this.trans_id = this.hasMany(models.Transaction);
  };
  return ConsumerVoucher;
};