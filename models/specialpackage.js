"use strict";

const moment = require('moment'); 
module.exports = (sequelize, DataTypes) => {
  var SpecialPackage = sequelize.define(
    "SpecialPackage",
    {
      specialPackage_id: {
        type: DataTypes.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true
      },
      merchant_id: DataTypes.INTEGER.UNSIGNED,
      applicable_restaurant: DataTypes.STRING,
      package_title: DataTypes.STRING,
      package_type: DataTypes.STRING,
      package_price: DataTypes.DECIMAL,
      item_price: DataTypes.DECIMAL,
      package_desc: DataTypes.STRING,
      package_menu: DataTypes.STRING,
      package_remark: {type: DataTypes.STRING, 
        get: function () {
          if(this.getDataValue('package_remark'))
          {
            return JSON.stringify(this.getDataValue('package_remark').split(';')); 

          }                                           
          else if(this.getDataValue('package_remark') === undefined)
              return ;
          else 
              return '[]';
        }},
      sales: DataTypes.INTEGER,
      quantity: DataTypes.INTEGER,
      package_get_limit: DataTypes.INTEGER,
      package_use_date_start: DataTypes.DATEONLY,
      package_use_date_end: DataTypes.DATEONLY,
      package_use_times: DataTypes.STRING,
      stackable: DataTypes.BOOLEAN,
      applicable: DataTypes.BOOLEAN,
      dinein_takeaway_flag: DataTypes.BOOLEAN,
      delivery_pickup_flag: DataTypes.BOOLEAN,
      applicable_item: DataTypes.STRING,
      publish_start: {type: DataTypes.DATE, 
        get: function () {
          if(this.getDataValue('publish_start'))
              return moment(this.getDataValue('publish_start'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');                                            
          else if(this.getDataValue('publish_start') === undefined)
              return ;
          else 
              return this.getDataValue('publish_start');
        }},
      publish_end: {type: DataTypes.DATE, 
        get: function () {
          if(this.getDataValue('publish_end'))
              return moment(this.getDataValue('publish_end'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');                                            
          else if(this.getDataValue('publish_end') === undefined)
              return ;
          else 
              return this.getDataValue('publish_end');
        }},
      use_weekday: DataTypes.BOOLEAN,
      use_weekend: DataTypes.BOOLEAN,
      use_holiday: DataTypes.BOOLEAN,
      publish: DataTypes.BOOLEAN,
      status: {type: DataTypes.INTEGER, 
        get: function () {
          
          if(this.getDataValue('status') >= 0){
            var status = ["Draft","In Review","Approved","Active","Expired","Terminated","Rejected"];
            
            if(status[this.getDataValue('status')])
              return status[this.getDataValue('status')];
            else
              return "Other"
          }else
            return this.getDataValue('status')
        }},
      reason: DataTypes.STRING,
      refundable: DataTypes.BOOLEAN,
    },
    {
      freezeTableName: true,
      tableName: "restaurant_special_package",

      timestamps: true,
      paranoid: true,

      getterMethods: {
          terms : function(){
            var terms = [];
            //Validity hours: Tuesday to Sunday: 12pm - 10pm Not valid on Monday.
            //Valid from now till 31 July 2018. (include weekend, weekday, holiday)
            //Limited to one usage per user
            //Limited to maximum of four (4) vouchers per customer
            //valid at all outlet
            //This voucher is not valid with other 
            //Voucher is not refundable
            //iEat app voucher is not exchangeable for cash
            //Not valid with other vouchers

            //Not valid for other drinks selection, only valid for KOPI.
            // Voucher can only use for selection item. / any item.

            // Voucher is not applicable for other voucher
            // Voucher is applicable to restaurant
            

            //stackable => same voucher use same time
            //applicable => diffent voucher use same time
              
            var include = "";
            if(this.getDataValue('use_weekday') && this.getDataValue('use_weekend') && this.getDataValue('use_holiday'))
              include = "";
            else{

              if(this.getDataValue('use_weekday'))
                include += "weekday";
              
              if(this.getDataValue('use_weekend'))
              {
                if(include != "") include += ", ";
                include += "weekend";
              }
                
              if(this.getDataValue('use_holiday'))
              {
                if(include != "") include += ", ";
                include += "holiday";
              }
            }
           
            if(moment().format('YYY-MM-DD') > moment(this.getDataValue('package_use_date_start')).format('YYY-MM-DD'))
                terms.push("Valid from now till "+moment(this.getDataValue('package_use_date_end')).format("DD MMMM YYYY")+"."+ ((include != "") ? " (Valid for "+include+" only)" : ""));
            else
                terms.push("Valid for redemption from "+moment(this.getDataValue('package_use_date_start')).format("DD MMMM YYYY")+" till "+moment(this.getDataValue('package_use_date_end')).format("DD MMMM YYYY")+"."+ ((include != "") ? " (Valid for "+include+" only)" : ""));


            if(this.getDataValue('dinein_takeaway_flag') && this.getDataValue('delivery_pickup_flag'))
              terms.push("Valid for dine in/take away and delivery/pick up")
            else if(this.getDataValue('dinein_takeaway_flag'))
              terms.push("Valid for dine in and take away")
            else if(this.getDataValue('delivery_pickup_flag'))
              terms.push("Valid for delivery and pick up")
            
            if(this.getDataValue('package_get_limit') > 0)
              terms.push("Maximum of "+this.getDataValue('package_get_limit')+" vouchers per customer")
              

            if(this.getDataValue('stackable'))
              terms.push("Voucher is stackable")
            else
              terms.push("Voucher is limited one per order")
            

            if(this.getDataValue('applicable'))
              terms.push("Voucher is applicable with other vouchers")
            else
              terms.push("Voucher is not applicable with other vouchers")
            
            var items = null;
            if(this.getDataValue('applicable_item'))
            {
              try{
                items = JSON.parse(this.getDataValue('applicable_item'));
              }catch(err){
                  console.log(err)
              }

            }

            if(this.getDataValue('restaurants'))
            {
              var selection = false;  
              var restaurant = "";
              var count = 0;

              this.getDataValue('restaurants').map(function(v){
                if(v.RestaurantSetting != null)
                {
                  count++;
                  if(restaurant != "") restaurant += ",";
                  restaurant += v.restaurant_name;
                  if(items != null)
                  {
                    if(items[v.restaurant_id].length > 0)
                      selection = true;
                  }
                }
              })

              if(count > 0)
              {
                if(count > 1)
                  terms.push("Voucher is applicable to "+restaurant);
                else
                  terms.push("Only applicable to "+restaurant);

                if(items){
                  if(selection)
                    terms.push("Voucher can only use for selection item.");
                  // else
                  //   terms.push("Voucher can only use for any item.");

                }
              }

            }
            
            
            if(this.getDataValue('package_type') != 'C' && this.getDataValue('package_menu'))
            {
              var menu = JSON.parse(this.getDataValue('package_menu'))
              if(menu.over)
                terms.push("Minimum spend of RM"+menu.over+" is required.");

            }
            
            if(this.getDataValue('refundable'))
              terms.push("Voucher will auto refund after expiry date")
            else
              terms.push("Voucher is not refundable")

            terms.push("Lolol app voucher is not exchangeable for cash")
            return terms;
            
          },
          package_flag: function(){
            if(this.getDataValue('refundable'))
            
              return ["Expired Auto Refund"]
            else if(this.getDataValue('refundable') === undefined)
              return ;
            else
              return []
            
          }
      }
    }
  );
//ALTER TABLE `restaurant_special_package` CHANGE `restaurant` `applicable_restaurant` VARCHAR(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
  SpecialPackage.associate = function(models) {
    // this.specialPackage_id = this.hasMany(models.Media, {foreignKey: 'specialPackage_id'});
    this.specialPackage_id = this.hasMany(models.ConsumerVoucher, {foreignKey: 'package_id'});
    // this.merchant_id = this.belongsTo(models.merchant, {foreignKey: 'merchant_id'});
    this.applicable_restaurant = this.hasMany(models.restaurant, {foreignKey: 'restaurant_id'});
    //this.applicable_restaurant = this.belongsToMany(models.restaurant, { foreignKey: 'restaurant_id', through: SpecialPackage});
  };

  SpecialPackage.prototype.toWeb = function(pw) {
    let json = this.toJSON();
    return json;
  };

  return SpecialPackage;
};