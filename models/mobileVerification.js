'use strict';
module.exports = (sequelize, DataTypes) => {

  var MobileVerification = sequelize.define('MobileVerification', {
   
    country_code: DataTypes.STRING,
    phone_no: DataTypes.STRING,
    veri_code: DataTypes.STRING,
    oper: DataTypes.STRING,
    oper_id: DataTypes.INTEGER,
    oper_from: DataTypes.STRING,
    expired_tms: DataTypes.STRING,
    status: DataTypes.STRING,
    ip_addrs: DataTypes.INTEGER,
    
    
  }, {
    freezeTableName: true,
    tableName: 'mobile_verification',

  });



  

  return MobileVerification;
};