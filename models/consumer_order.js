const moment = require('moment');
'use strict';
module.exports = (sequelize, DataTypes) => {

    var ConsumerOrder = sequelize.define('ConsumerOrder', {
  
      id: {type:DataTypes.INTEGER ,primaryKey: true,autoIncrement: true} ,
      tax_invoice:DataTypes.STRING,
      consumer_id: DataTypes.INTEGER.UNSIGNED,
      fullname:DataTypes.STRING,
      contact:DataTypes.STRING,
      restaurant_id:DataTypes.INTEGER.UNSIGNED,
      restaurant_name:DataTypes.STRING,
      restaurant_address:DataTypes.STRING,
      restaurant_latitude:DataTypes.STRING,
      restaurant_longitude:DataTypes.STRING,
      restaurant_contact:DataTypes.STRING,
      order_type:DataTypes.STRING,
      order_date:'TIMESTAMP',
      estimated_datetime:'TIMESTAMP',
      place_order_date:'TIMESTAMP',
      menu:DataTypes.STRING,
      total:DataTypes.DECIMAL,
      payment:DataTypes.STRING,
      voucher_amount:DataTypes.DECIMAL,
      remark:DataTypes.STRING,
      status:DataTypes.INTEGER,
      payment_time:'TIMESTAMP',
      cancel_time:'TIMESTAMP',
      reason:DataTypes.STRING,
      dinein_session:DataTypes.STRING,
      secret_code:DataTypes.INTEGER,
      restaurant_discount:DataTypes.STRING,
      sys_remark:DataTypes.TEXT,
      extra:DataTypes.TEXT,
      refer_order_id:DataTypes.STRING,
      discount_amount:DataTypes.DECIMAL,
      voucher_item:DataTypes.STRING,
      review_id:DataTypes.STRING,
      update_id:DataTypes.INTEGER,
    }, {
      freezeTableName: true,
      tableName: 'consumer_order',
      timestamps: true,
      createdAt: 'place_order_date',
      updatedAt: false, 
      getterMethods: {
  
          dine_in_cancel:function(){
  
              let cancel_amount = 0;
  
              if(this.getDataValue('order_type')=='dine_in')
              {
                 
                  if(this.getDataValue('menu'))
                  {
                      let menu  = JSON.parse(this.getDataValue('menu'));
  
                      menu.forEach(element=>{
                          if(element.serve ==3 )
                          {
                              cancel_amount +=parseFloat(element.total);
                          }
                      })
  
                  }
                  
              }
  
              return cancel_amount;
  
          },
        pre_order:function(){
  
          if(moment(this.getDataValue('place_order_date')).isSame(moment(this.getDataValue('order_date'))))
              return 0;
          
          return 1;
      
        },
        status_text:  function() {
         
          if(this.getDataValue('order_type'))
          {
  
              var object = {
                  delivery:{
                      '0' :'PAYMENT PROCESSING',
                      '1' :"AWAITING CONFIRMATION",
                      '2' :"CONFIRMED",
                      '3' :"DISH READY",
                      '4' :"AWAITING DELIVERY",
                      '5' :"IN-DELIVERY",
                      '6' :"COMPLETED",
                      '7' :"CANCELLED by Merchant",
                      '8' :"CANCELLED by Client",
                      '9' :"CANCELLED by System",
                      '10':"Submitted Refund",
                      '11':"Agreed Refund (by merchant)",
                      '12':"Agreed Refund (by admin)",
                      '13':"Processing Refund",
                      '14':"Refunded",
                      '15':"Rejected Refund",
                  },
                  pick_up:{
                      '0' :'PAYMENT PROCESSING',
                      '1' :"AWAITING CONFIRMATION",
                      '2' :"CONFIRMED",
                      '3' :"DISH READY",
                      '6' :"COMPLETED",
                      '7' :"CANCELLED by Merchant",
                      '8' :"CANCELLED by Client",
                      '9' :"CANCELLED by System",
                      '10':"Submitted Refund",
                      '11':"Agreed Refund (by merchant)",
                      '12':"Agreed Refund (by admin)",
                      '13':"Processing Refund",
                      '14':"Refunded",
                      '15':"Rejected Refund",
                  },
                  dine_in:{
                      '1' :"AWAITING CONFIRMATION",
                      '2' :"CONFIRMED",
                      '6' :"COMPLETED",
                      '7' :"CANCELLED by Merchant",
                      '8' :"CANCELLED by Client",
                      '9' :"CANCELLED by System",
                      '10':"Submitted Refund",
                      '11':"Agreed Refund (by merchant)",
                      '12':"Agreed Refund (by admin)",
                      '13':"Processing Refund",
                      '14':"Refunded",
                      '15':"Rejected Refund",
                  },
                  take_away:{
                      '1' :"AWAITING CONFIRMATION",
                      '2' :"CONFIRMED",
                      '3' :"DISH READY",
                      '6' :"COMPLETED",
                      '7' :"CANCELLED by Merchant",
                      '8' :"CANCELLED by Client",
                      '9' :"CANCELLED by System",
                      '10':"Submitted Refund",
                      '11':"Agreed Refund (by merchant)",
                      '12':"Agreed Refund (by admin)",
                      '13':"Processing Refund",
                      '14':"Refunded",
                      '15':"Rejected Refund",
                  }
              }
      
              return object[this.getDataValue('order_type')][this.status];
  
          }
  
          return null;
          
  
        },
        order_date: function () {
          if(this.getDataValue('order_date'))
              return moment(this.getDataValue('order_date'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
          else if(this.getDataValue('order_date') === undefined)
              return ;
          else 
            return null;
        },
        estimated_datetime: function () {
          if(this.getDataValue('estimated_datetime'))
              return moment(this.getDataValue('estimated_datetime'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
          else if(this.getDataValue('estimated_datetime') === undefined)
              return ;
          else 
            return null;
        },
        cancel_time: function () {
          if(this.getDataValue('cancel_time'))
              return moment(this.getDataValue('cancel_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
          else if(this.getDataValue('cancel_time') === undefined)
              return ;
          else 
            return null;
        },
        place_order_date: function () {
          if(this.getDataValue('place_order_date'))
              return moment(this.getDataValue('place_order_date'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
          else if(this.getDataValue('place_order_date') === undefined)
              return ;
          else 
            return null;
        },
        packaging :function(){
            try{
  
              let menu = JSON.parse(this.getDataValue('menu'));
  
              let fees = 0.00;
  
              menu.forEach(element => {
  
                  if(this.getDataValue('order_type')!='dine_in')
                  {   
                      if(element.packaging)
                          fees +=parseFloat(element.packaging)
                  }else
                  {
                      if(element.packaging && element.serve!=3)
                          fees +=parseFloat(element.packaging)
                  }
                  
              });
  
              return fees
  
            }catch(err)
            {
                return 0.00;
            }
            
        },
        subtotal:function(){
  
          try{
              let menu = JSON.parse(this.getDataValue('menu'));
  
              let fees = 0.00;
  
              menu.forEach(element => {
  
                  if(this.getDataValue('order_type')!='dine_in')
                      fees +=parseFloat(element.total)-parseFloat(element.packaging);
                  else
                  {
                      if(element.serve!=3)
                      {
                          if(element.packaging)
                              fees +=parseFloat(element.total)-parseFloat(element.packaging);
                          else
                              fees +=parseFloat(element.total)
                          
                              
                      }
                  }
                  
              });
              return fees
  
            }catch(err)
            {
              return 0.00;
            }
        }
    },
  
    });

  ConsumerOrder.associate = function(models) {

    // this.restaurant_id = this.belongsTo(models.restaurant, {foreignKey: 'restaurant_id'});

    // ConsumerOrder.belongsTo(models.Consumer, {
    //   foreignKey: 'consumer_id'
    // });

    // ConsumerOrder.hasMany(models.ConsumerVoucher, {
    //   foreignKey: 'order_id',
    // });

    // ConsumerOrder.hasMany(models.ConsumerOrderTime, {
    //   foreignKey: 'order_id',
    //   targetKey:'id'
    // });

    ConsumerOrder.hasOne(models.ConsumerOrderDelivery, {
        foreignKey: 'order_id',
      });


    // this.dinein_session = this.belongsTo(models.ConsumerTable, {foreignKey: 'dinein_session',targetKey:'session_id'});
    
    
  };

  return ConsumerOrder;
};