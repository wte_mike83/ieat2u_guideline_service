'use strict';

module.exports = (sequelize, DataTypes) => {
  var SuggestBusiness = sequelize.define('SuggestBusiness', {

    business_id: {type:DataTypes.INTEGER ,primaryKey: true,autoIncrement: true} ,
    consumer_id: DataTypes.INTEGER.UNSIGNED,
    restaurant_name: DataTypes.STRING,
    restaurant_address: DataTypes.STRING,
    merchant_name:DataTypes.STRING,
    country_code: DataTypes.STRING,
    phone_no: DataTypes.STRING,
    password: DataTypes.STRING,
    merchant_id:DataTypes.INTEGER.UNSIGNED,
		status: DataTypes.STRING,
		remark: DataTypes.STRING,

  }, {
    freezeTableName: true,
    tableName: 'suggest_business',
  });

  SuggestBusiness.associate = function(models) {

    this.id = this.hasMany(models.Media, {foreignKey: 'business_id'});
    
  };

  return SuggestBusiness;
};