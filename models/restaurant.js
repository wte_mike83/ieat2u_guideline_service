'use strict';

module.exports = (sequelize, DataTypes) => {
    var restaurant = sequelize.define('restaurant', {
        restaurant_id                   : {type:DataTypes.INTEGER.UNSIGNED ,primaryKey: true,autoIncrement: true},
    	merchant_id				        : DataTypes.INTEGER.UNSIGNED,
        restaurant_name    		        : DataTypes.STRING,
        restaurant_desc    		        : {type: DataTypes.STRING, allowNull: true},
        restaurant_type    		        : DataTypes.STRING,
        restaurant_address1    	        : DataTypes.STRING,
        restaurant_address2    	        : {type: DataTypes.STRING, allowNull: true},
        restaurant_city    		        : DataTypes.STRING,
        restaurant_state    	        : DataTypes.STRING,
        restaurant_country    	        : DataTypes.STRING,
        restaurant_zone    		        : {type: DataTypes.STRING, allowNull: true},
        restaurant_postcode    	        : DataTypes.STRING,
        restaurant_latitude    	        : DataTypes.DECIMAL(15, 10),
        restaurant_longitude            : DataTypes.DECIMAL(15, 10),
        restaurant_contact_no           : {type: DataTypes.STRING, allowNull: false, validate: { len: {args: [8, 12], msg: "Phone number invalid."}, isNumeric: { msg: "not a valid phone number."} }},
        contact_country_code            : DataTypes.STRING,
        restaurant_business_phone       : {type: DataTypes.STRING, allowNull: true},
        business_phone_country_code     : {type: DataTypes.STRING, allowNull: true},
        restaurant_fax                  : {type: DataTypes.STRING, allowNull: true},
        fax_country_code                : {type: DataTypes.STRING, allowNull: true},
        automated_call_no               : {type: DataTypes.STRING, allowNull: true},
        automated_call_country_code     : {type: DataTypes.STRING, allowNull: true},
        restaurant_website              : {type: DataTypes.STRING, allowNull: true},
        isVerified 	                    : DataTypes.BOOLEAN,
        acc_active      		        : DataTypes.BOOLEAN,
        headquarters 		            : DataTypes.BOOLEAN,
        restaurant_recommended 		    : DataTypes.BOOLEAN,
        view_count 		                : DataTypes.INTEGER,
        ambience                        : {type: DataTypes.STRING, allowNull: true,
                                        get: function () {
                                            if(this.getDataValue('ambience'))
                                                return this.getDataValue('ambience').split(', ');
                                            else if(this.getDataValue('ambience') === undefined)
                                                return ;
                                            else 
                                                return [];
                                        }},
        amenity                         : {type: DataTypes.STRING, allowNull: true,
                                        get: function () {
                                            if(this.getDataValue('amenity'))
                                                return this.getDataValue('amenity').split(', ');
                                            else if(this.getDataValue('amenity') === undefined)
                                                return ;
                                            else 
                                                return [];
                                        }},
        cuisine                         : {type: DataTypes.STRING, allowNull: true,
                                        get: function () {
                                            if(this.getDataValue('cuisine'))
                                                return this.getDataValue('cuisine').split(', ');
                                            else if(this.getDataValue('cuisine') === undefined)
                                                return ;
                                            else 
                                                return [];
                                        }},
        category                        : {type: DataTypes.STRING, allowNull: true,
                                        get: function () {
                                            if(this.getDataValue('category'))
                                                return this.getDataValue('category').split(', ');
                                            else if(this.getDataValue('category') === undefined)
                                                return ;
                                            else 
                                                return [];
        }},
        busy_tms                        : 'TIMESTAMP',
        temporary_close_from            : 'TIMESTAMP',
        temporary_close_to              : 'TIMESTAMP',
        menu_category_index             : {type: DataTypes.STRING, allowNull: true},
        total_vote                      : DataTypes.INTEGER,
        total_rate                      : DataTypes.INTEGER,
        announcement                    : {type: DataTypes.STRING, allowNull: true},
        food_preference                 : DataTypes.STRING
    }, {
        freezeTableName: true,
        tableName: 'restaurant',

        getterMethods: {
           
            /*bookmark : function(){
                return 0;
            },
            contact_no:function(){
                if(this.restaurant_contact_no)
                {
                    return this.contact_country_code +this.restaurant_contact_no;
                }else{
                    return;
                }
            },
            business_phone:function(){
                if(this.restaurant_business_phone)
                {
                    return this.business_phone_country_code + this.restaurant_business_phone;
                }else if(this.restaurant_business_phone == "" || this.restaurant_business_phone == null){
                    return null;
                }else{
                    return;
                }
            },
            fax:function(){
                if(this.restaurant_fax)
                {
                    return this.fax_country_code + this.restaurant_fax;
                }else if(this.restaurant_fax == "" || this.restaurant_fax == null){
                    return null;
                }else{
                    return;
                }
                
            },
            address: function(){
                if(this.restaurant_address1)
                    return this.restaurant_address1 +", "+ this.restaurant_postcode +", "+ this.restaurant_country;
                else
                    return;
            }*/
            
            
        },
    });
    restaurant.associate = function(models) {
        this.restaurant_id = this.hasMany(models.SpecialPackage, {foreignKey: 'applicable_restaurant'});
        
        // this.merchant_id = this.belongsTo(models.merchant, {foreignKey: 'merchant_id'});
        // this.restaurant_id = this.hasOne(models.RestaurantSetting, {foreignKey: 'restaurant_id'});
        // this.restaurant_id = this.hasMany(models.RestaurantBusinessHour, {foreignKey: 'restaurant_id'});
        // this.restaurant_id = this.hasMany(models.Media, {foreignKey: 'restaurant_id'});
        // // this.restaurant_id = this.hasMany(models.RestaurantSales, {foreignKey: 'restaurant_id'});
        // // this.restaurant_id = this.hasMany(models.ItemMenu, {foreignKey: 'restaurant_id'});
        // this.restaurant_id = this.hasMany(models.consumer_choice, {foreignKey: 'restaurant_id'});
        // this.restaurant_id = this.hasMany(models.viewed_history, {foreignKey: 'restaurant_id'});
    };

    restaurant.prototype.toWeb = function (pw) {
        let json = this.toJSON();
        return json;
    };

    return restaurant;
};