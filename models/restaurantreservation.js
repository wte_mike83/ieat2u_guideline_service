'use strict';

module.exports = (sequelize, DataTypes) => {

  const moment = require('moment'); 
  var RestaurantReservation = sequelize.define('RestaurantReservation', {

    reserve_id:{type:DataTypes.INTEGER.UNSIGNED ,primaryKey: true,autoIncrement: true} ,
    restaurant_id   : DataTypes.INTEGER.UNSIGNED,
    consumer_id   : DataTypes.INTEGER.UNSIGNED,
    reserve_person:DataTypes.INTEGER,
    reserve_date:DataTypes.DATEONLY,
    reserve_time:DataTypes.TIME,
    contact_person:DataTypes.STRING,
    mobile_number:DataTypes.STRING,
    reserve_remark:DataTypes.STRING,
    reserve_uuid:DataTypes.STRING,
    reserve_status:DataTypes.STRING,
    awaiting_time:{type: DataTypes.DATE,
    table_id: DataTypes.INTEGER.UNSIGNED,
      get: function () {
          if(this.getDataValue('awaiting_time'))
              return moment(this.getDataValue('awaiting_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
          else if(this.getDataValue('awaiting_time') === undefined)
              return ;
          else 
            return null;
    }},
    confirmed_time:{type: DataTypes.DATE,
      get: function () {
          if(this.getDataValue('confirmed_time'))
              return moment(this.getDataValue('confirmed_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
          else if(this.getDataValue('confirmed_time') === undefined)
              return ;
          else 
            return null;
    }},
    completed_time:{type: DataTypes.DATE,
      get: function () {
          if(this.getDataValue('completed_time'))
              return moment(this.getDataValue('completed_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
          else if(this.getDataValue('completed_time') === undefined)
              return ;
          else 
            return null;
    }},
    cancelled_time:{type: DataTypes.DATE,
      get: function () {
          if(this.getDataValue('cancelled_time'))
              return moment(this.getDataValue('cancelled_time'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
          else if(this.getDataValue('cancelled_time') === undefined)
              return ;
          else 
              return null;
    }},
    reason:DataTypes.STRING,
    reserve_late_time:DataTypes.TIME,
  }, {
    freezeTableName: true,
    tableName: 'restaurant_reservation',
    timestamps: false,

  });

  RestaurantReservation.associate = function(models) {
    
    // associations can be defined here
    this.restaurant_id = this.belongsTo(models.restaurant, {foreignKey: 'restaurant_id'});
    this.table_id = this.belongsTo(models.RestaurantTable, {foreignKey: 'table_id'});
    //this.reserve_uuid = this.hasOne(models.ConsumerTable, {foreignKey: 'reserve_uuid'});
  };


  RestaurantReservation.prototype.toWeb = function (pw) {
    return this.toJSON();
  };

  return RestaurantReservation;
};