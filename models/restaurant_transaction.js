'use strict';
const moment = require('moment'); 
module.exports = (sequelize, DataTypes) => {
  var RestaurantTransaction = sequelize.define('RestaurantTransaction', {

    trans_id: {type:DataTypes.INTEGER ,primaryKey: true,autoIncrement: true} ,
    restaurant_id: DataTypes.INTEGER.UNSIGNED,
    consumer_id: DataTypes.INTEGER.UNSIGNED,
    order_id: DataTypes.INTEGER.UNSIGNED,
    order_type: DataTypes.INTEGER,
    trans_type:DataTypes.STRING,
    trans_status:DataTypes.STRING,
    trans_amount:DataTypes.DECIMAL,
    trans_no:DataTypes.STRING,
    trans_method:DataTypes.STRING,
    fee: DataTypes.DECIMAL,
    prev_balance: DataTypes.DECIMAL,
    current_balance: DataTypes.DECIMAL,
    trans_date:'TIMESTAMP',
    trans_update_date:'TIMESTAMP',
    remark:DataTypes.TEXT,
    refer:DataTypes.INTEGER,
    trans_extra:DataTypes.TEXT,
    batch_time:DataTypes.DATE,

  }, {
    freezeTableName: true,
    tableName: 'restaurant_transaction',
    createdAt: 'trans_date',
    updatedAt: 'trans_update_date',
    getterMethods: {
      date:function(){
          return moment(this.trans_date).format('Y/MM/DD');
      },
      trans_date:function(){
        if(this.getDataValue('trans_date'))
          return moment(this.getDataValue('trans_date'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
        else if(this.getDataValue('trans_date') === undefined)
            return ;
        else 
          return null;
      },
      order_type:  function() {
       
        if(this.getDataValue('order_type'))
        {
          var order_type = {
                '1' :"Cash Voucher",
                '2' :"Food Voucher",
                '3' :"Delivery",
                '4' :"Pick Up",
                '5' :"Dine In",
                '6' :"c2m Payment",
                '7' :"Scan and Pay"
            }

          return order_type[this.getDataValue('order_type')];
        }
        else
          return null;
      },
      amount: function() {
        let amount = 0.00;

        amount = this.getDataValue('trans_amount') - this.getDataValue('fee'); 

        return amount.toFixed(2);
      }
    }
  });

  RestaurantTransaction.associate = function(models) {
  
  };

  return RestaurantTransaction;
};