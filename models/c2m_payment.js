'use strict';
module.exports = (sequelize, DataTypes) => {
  var c2m_payment = sequelize.define('c2m_payment', {
        c2m_id          : {type:DataTypes.INTEGER.UNSIGNED ,primaryKey: true,autoIncrement: true},
        consumer_id     : DataTypes.INTEGER.UNSIGNED,
        restaurant_id   : DataTypes.INTEGER.UNSIGNED,
        payment_code    : DataTypes.INTEGER.UNSIGNED,
        amount          : DataTypes.DECIMAL,
        description     : DataTypes.STRING,

    }, {
        freezeTableName: true,
        tableName: 'c2m_payment',
    });

  //   c2m_payment.associate = function(models) {
  //   this.restaurant_id = this.belongsTo(models.restaurant, {foreignKey: 'restaurant_id'});
  //   // this.restaurant_id = this.hasMany(models.RestaurantReservation, {foreignKey: 'restaurant_id'});
  // };

  c2m_payment.prototype.toWeb = function (pw) {
      let json = this.toJSON();
      return json;
  };

  return c2m_payment;
};