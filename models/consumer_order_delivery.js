'use strict';
module.exports = (sequelize, DataTypes) => {

  var ConsumerOrderDelivery = sequelize.define('ConsumerOrderDelivery', {

    id: {type:DataTypes.INTEGER ,primaryKey: true,autoIncrement: true} ,
    order_id:DataTypes.INTEGER.UNSIGNED,
    address:DataTypes.STRING,
    latitude:DataTypes.DECIMAL,
    longitude:DataTypes.DECIMAL,
    others:DataTypes.STRING,
    fees:DataTypes.DECIMAL,
    rider_detail:DataTypes.STRING,
    order_rider_uuid:DataTypes.STRING,

  }, {
    freezeTableName: true,
    tableName: 'consumer_order_delivery',
    timestamps:false

  });
  ConsumerOrderDelivery.associate = function(models) {
    // associations can be defined here
    this.order_id = this.belongsTo(models.ConsumerOrder, {foreignKey: 'order_id'});
  };
  return ConsumerOrderDelivery;
};