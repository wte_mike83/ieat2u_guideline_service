'use strict';


module.exports = (sequelize, DataTypes) => {
	var MerchantGuideline = sequelize.define('MerchantGuideline', {

		tutorial_id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
		title: DataTypes.STRING,
		type: DataTypes.STRING,
		tag: DataTypes.STRING,
		path  : {type: DataTypes.STRING, 
			get: function () {
					if(this.getDataValue('path'))
							return CONFIG.filePath +'/images/guideline/'+ this.getDataValue('path');
					else if(this.getDataValue('profile_pic') === undefined)
							return ;
					else 
							return CONFIG.filePath + '/images/media/default.png';
							//return null;

		}},


	}, {
		freezeTableName: true,
		tableName: 'merchant_guideline',
	});

	MerchantGuideline.associate = function(models) {
    // associations can be defined here
  };

	// Merchant.prototype.toWeb = function (pw) {
	// let json = this.toJSON();
	// return json;
	// };
	
	MerchantGuideline.prototype.toWeb = function (pw) {
		let json = this.toJSON();
		delete json['path'];

		return json;
	};





	
  return MerchantGuideline;
};