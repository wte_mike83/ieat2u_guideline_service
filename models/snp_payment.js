'use strict';
module.exports = (sequelize, DataTypes) => {
  var snp_payment = sequelize.define('snp_payment', {
        snp_id          : {type:DataTypes.INTEGER.UNSIGNED ,primaryKey: true,autoIncrement: true},
        consumer_id     : DataTypes.INTEGER.UNSIGNED,
        restaurant_id		: DataTypes.INTEGER.UNSIGNED,
        amount          : DataTypes.DECIMAL,
        status          : DataTypes.INTEGER,
        description     : DataTypes.STRING,
        payment_time    : 'TIMESTAMP',

    }, {
        freezeTableName: true,
        tableName: 'snp_payment',

        timestamps: true,
        createdAt: 'payment_time',
        updatedAt: false,
    });

    snp_payment.associate = function(models) {

    // this.c2m_id = this.hasMany(models.Transaction, {foreignKey: 'order_id'});

    };

    snp_payment.prototype.toWeb = function (pw) {
      let json = this.toJSON();
      return json;
  };

  return snp_payment;
};