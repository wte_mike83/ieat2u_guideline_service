'use strict';
const jwt    = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
	var Merchant = sequelize.define('merchant', {

		merchant_id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
		email: DataTypes.STRING,
		password: DataTypes.STRING,
		display_name: DataTypes.STRING,
		phone_no: DataTypes.STRING,
		country_code: DataTypes.STRING,
		phone_verification_code: DataTypes.STRING,
		reset_password_otp: DataTypes.STRING,
		otp_expired_tms:'TIMESTAMP',
		email_verification_flag: DataTypes.INTEGER,
		email_verification_code: DataTypes.STRING,
		first_time_login: DataTypes.TINYINT,
		role: DataTypes.STRING,
		acc_active: DataTypes.TINYINT,
		profile_pic: DataTypes.STRING,
		profile_size: DataTypes.STRING,
		profile_type: DataTypes.STRING,
		last_login: 'TIMESTAMP',
		remember_token: DataTypes.STRING,
    notif_bell: 'TIMESTAMP',
		main_merchant: DataTypes.INTEGER,
		permission: DataTypes.STRING,
		agent_id: DataTypes.INTEGER,
		agent_percentage: DataTypes.DECIMAL,
		is_verified: DataTypes.TINYINT,
		
	}, {
		freezeTableName: true,
		tableName: 'merchant',
		timestamps: true,
		paranoid: true,
	});

	Merchant.associate = function(models) {
    // associations can be defined here
  };

	// Merchant.prototype.toWeb = function (pw) {
	// let json = this.toJSON();
	// return json;
	// };
	
	Merchant.prototype.toWeb = function (pw) {
		let json = this.toJSON();
		delete json['password'];

		return json;
	};

  Merchant.prototype.getJWT = function (device_id) {
    let expiration_time = parseInt(CONFIG.jwt_expiration);
    return "Bearer "+jwt.sign({merchant_id:this.merchant_id,device_id:device_id}, CONFIG.jwt_encryption_merchant, {expiresIn: expiration_time});
	};

	
  return Merchant;
};