'use strict';
const jwt    = require('jsonwebtoken');
module.exports = (sequelize, DataTypes) => {

  var Admin = sequelize.define('Admin', {
   
    
    admin_id: {type:DataTypes.INTEGER ,primaryKey: true,autoIncrement: true} ,
    email: DataTypes.STRING,
    username: DataTypes.STRING,
    country_code: DataTypes.STRING,
    phone_no: DataTypes.STRING,
    profile_name: DataTypes.STRING,
    password: DataTypes.STRING,
    acc_active: DataTypes.STRING,
    profile_pic: DataTypes.STRING,
    profile_checksum: DataTypes.STRING,
  }, {
    freezeTableName: true,
    tableName: 'admin',
    getterMethods: {
       profile_url:  function() {

         if(this.getDataValue('profile_pic') != null)
           return CONFIG.filePath +'profile/'+this.getDataValue('profile_pic');
         else
          return CONFIG.filePath +'profile/default-user.png';

       },
      
    },

  });

  Admin.prototype.toWeb = function (pw) {
    let json = this.toJSON();
    delete json['password'];

    return json;
  };

  Admin.prototype.getJWT = function () {
    let expiration_time = parseInt(CONFIG.jwt_expiration);
    return "Bearer "+jwt.sign({admin_id:this.admin_id}, CONFIG.jwt_encryption, {expiresIn: expiration_time});
  };

  
  
  return Admin;
};