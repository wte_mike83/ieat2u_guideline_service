

// for review image,
const MerchantGuideline  = require('../models').MerchantGuideline;
const sequelize = require('../models').sequelize;
const multer  = require('multer');
const fs = require('fs');

const dir = CONFIG.storePath;   //C:\Users\IEAT User\Documents\guideline
const maxSize = 50 * 1024 * 1024;
const maxVideoSize = 500 * 1024 * 1024;
 
const UploadFile = async function (req, res) {
 

    const path = require('path')

//console.log("dir name"+__dirname);
    var storage = multer.diskStorage({
        // destination: (req, file, cb) => {
        //   cb(null, dir)
        // },
        destination: function (req, file, callback) {
          
            if(file.mimetype == 'video/mp4')
            callback(null, path.join(dir,'video'))
            else if(file.mimetype == 'application/pdf')
            callback(null, path.join(dir,'pdf'))
          },
        filename: (req, file, cb) => {
         // cb(null, file.originalname.split('.').shift() + '_' + Date.now() + '.' + file.originalname.split('.').pop())
         cb(null, generateRandomKey(5) + Date.now() +'.' + file.originalname.split('.').pop())
        }
      });


      var upload = multer({
        storage: storage, 
        limits: maxSize,
        fileFilter: function (req, file, cb) {
            if (file.mimetype !== 'application/pdf'&& file.mimetype !== 'video/mp4' ) {
                console.log("here"+file.mimetype)
                return cb(new Error('Only pdf or video file are allowed'))
              }
            cb(null, true);
        }}
    ).single('file');

     

      upload(req, res, function (err) {
        var title = req.body.title;
        var tag = req.body.tag;

        try
        {
        if(!title)
        title = req.file.originalname.split('.').shift();
         }catch(err) {
            console.log("error"+err)
        }

        if (err) {
            console.log("error"+err)

            return ReE(res, err)

        } else {

          if (req.file) {

                   console.log("single pdf name : " + JSON.stringify(req.file))
                   console.log("req body : " + JSON.stringify(req.body))

                   var path;

                if(req.file.mimetype=='application/pdf')
                    path = "pdf/"+req.file.filename
                 else if(req.file.mimetype=='video/mp4')  
                    path = "video/"+req.file.filename

                   let condition = {
                    title: title,
                    tag: tag,
                    type: req.file.mimetype,
                    path: path
                 }

             
                

                 MerchantGuideline.create(condition).then((list)=>{
                    
           
                        return ReS(res, 'File Uploaded Successfully.', {
                            data: list.toWeb()
                        }, 200);
                


                    }).catch((err) => {

                        return ReE(res, 'invalid file');
                    });


          }else
          {

            
            return ReE(res, 'please insert file');

          }

         
         
        }
    })
}
module.exports.UploadFile = UploadFile;


const modifylist = async function (req, res) {

    const path = require('path')

    var storage = multer.diskStorage({

        destination: function (req, file, callback) {
          
            callback(null, path.join(dir,'temp'))

          },
        filename: (req, file, cb) => {
         // cb(null, file.originalname.split('.').shift() + '_' + Date.now() + '.' + file.originalname.split('.').pop())
         cb(null, generateRandomKey(5) + Date.now() +'.' + file.originalname.split('.').pop())
        }
      });


      var upload = multer({
        storage: storage, 
        limits: maxSize,
        fileFilter: function (req, file, cb) {
            if (file.mimetype !== 'application/pdf'&& file.mimetype !== 'video/mp4' ) {
           
                return cb(new Error('Only pdf or video file are allowed'))
              }
            cb(null, true);
        }}
    ).single('file');


    
    upload(req, res, function (err) {
    
    //global function

        fsDeleteFile = function(){
            fs.unlink(CONFIG.storePath +"/temp/"+req.file.filename,function(err){
                if(err) return console.log(err);
                console.log('temp file  deleted successfully');                   
            }); 

        }

        // if(!req.body.id)
        // return ReE(res, errors,400);

        var id = req.body.id

        

        //checkBody
        req.checkBody({
          'id': {
              notEmpty: true,
              isInt:{
                  errorMessage:'Invalid Tutorial Id'
              }
  
          }
  
  
      })

          
    var errors = req.validationErrors(); // get validation error 

    if (errors) 
    {
      
        return ReE(res, errors,400); // return first error to merchant
    }


    
        var title = req.body.title;
        var tag = req.body.tag;



        if (err) {
            console.log("error"+err)

            return ReE(res, err)

        }

    
            MerchantGuideline.findOne({
                where: {tutorial_id: id},
                attributes: ['tutorial_id','path','type']
              }).then(guideline => {
                console.log("search tutorial id:"+JSON.stringify(guideline));

                if(guideline) {
                var condition ={};
                if(title)
                condition.title = req.body.title;
                if(tag)
                condition.tag = req.body.tag;
             

                
                if (req.file) {        
                    console.log("show file"+JSON.stringify(req.file))

                    

                        if(guideline.type == req.file.mimetype)
                         {   
                             console.log("same file type")

                  
                    const filepath = req.file.destination;
                    try {
                        if (fs.existsSync(filepath)) {
                            console.log("file exist = "+fs.existsSync(filepath))
                            console.log("file destination = "+filepath)
                         
                  
                                     
                        guideline.update(
                        condition,
                        { where: { tutorial_id: id } }
                          ).then(result =>{

                            var oldfilepath = guideline.path.split( '/' );

                            var oldPath = CONFIG.storePath +"/temp/"+req.file.filename;
                            var newPath = CONFIG.storePath +"/"+oldfilepath[ oldfilepath.length - 2 ]+"/"+oldfilepath[ oldfilepath.length - 1 ] ;
                            //var newPath = guideline.path;
                           
                            fs.rename(oldPath, newPath, function (err) {
                            if (err) throw err
                            console.log('Successfully move to file')
                            })

                         // console.log("result here"+result)
                          return ReS(res, 'Successfully edit', 200);
      
                
        
                        }).catch((err) => {
        
                            return ReE(res,err,400);
                        });
        
        
                        }
                        } catch(err) {
                        console.error(err)
                        return ReE(res,err,400);
                        }
               


                }else
                {
                    console.error("wrong type")

                    fsDeleteFile();

                   return ReE(res,"wrong file type with previous file, please upload again ",400);
                    }            
              } else
              {
                  //no file upload
                guideline.update(
                    condition,
                    { where: { tutorial_id: id } }
                      ).then(result =>{


                      return ReS(res, 'Successfully edit', 200);
  
            
    
                    }).catch((err) => {
    
                        return ReE(res,err,400);
                    });
    

              }


            } else
            {
                console.error("no tutorial id found")

                if(req.file){
                    fsDeleteFile();                      
                }
               return ReE(res,"no tutorial id found",400);
            }


              
              }).catch((err) => {

                return ReE(res,err,400);
            });



          
        //   else
        //   {


        //     guideline.update(
        //         condition,
        //         { where: { tutorial_id: id } }
        //           ).then(result =>{


        //          // console.log("result here"+result)
        //           return ReS(res, 'Successfully edit b', 200);

        

        //         }).catch((err) => {

        //             return ReE(res,err,400);
        //         });


        //   }

         
         
        
    })





    //return ReE(res,"missing input",400);
}
module.exports.modifylist = modifylist;

// const UploadVideo = async function (req, res) {
 
 
    
//     var storage = multer.diskStorage({
//         destination: (req, file, cb) => {
//           cb(null, dir)
//         },
//         filename: (req, file, cb) => {
//           cb(null, file.originalname.split('.').shift() + '_' + Date.now() + '.' + file.originalname.split('.').pop())
//         }
//       });



//       var upload = multer({
//         storage: storage, 
//         limits: maxVideoSize,
//         fileFilter: function (req, file, cb) {
//             if (file.mimetype !== 'videoa/mp4' ) {
//                 console.log("here"+file.mimetype)
//                 console.log("here"+JSON.stringify(file))
//                 return cb(new Error('Only image file are allowed'))
//               }
//             cb(null, true);
//         }}
//     ).single('video');



//       upload(req, res, function (err) {


//         if (err) {
//             console.log("error"+err)
//             return ReS(res, 'error.');

//         } else {

//           if (req.files) {

//             console.log("video: " + req.files)

//              return ReS(res, 'Video Uploaded Successfully.');
//           }

//           return ReS(res, 'error.');
//         }
//     })
// }
// module.exports.UploadVideo = UploadVideo;






const DeleteFile = async function (req, res) {
    

    var tutorial_id = req.body.id

      //checkBody
      req.checkBody({
        'id': {
            notEmpty: true,
            isInt:{
                errorMessage:'Invalid Id'
            }

        }


    })

    
    
    var errors = req.validationErrors(); // get validation error 

    if (errors) 
    {
        return ReE(res, errors,400); // return first error to merchant
    }


    //sql 
    MerchantGuideline.destroy({
        where: {tutorial_id: tutorial_id},

    }).then(record => {


        if(record === 1){   
            return ReS(res, 'Deleted successfully', 200);    
        }
        else
        {
            return ReE(res, 'record not found');
        }



    }).catch(err => {
        return ReE(res, err)
    })



}
module.exports.DeleteFile = DeleteFile;


const getGuidelineList = async function (req, res) {

    var title = req.body.search;
    var filetype = req.body.type;

    req.checkBody({
        'type': {
            optional: true,
            isIn: {
                options: [
                    ['pdf', 'mp4']
                ],
                errorMessage: 'Only mp4 or pdf',
            }

        },
        'search': {
            // optional: req.body.period !== 'weekly',
            optional: true,
            errorMessage: 'search error'
        }


    })



    var errors = req.validationErrors(); // get validation error 

    if (errors) {
        return ReE(res, errors, 400); // return first error to merchant
    }



    var condition = {};
    if (title)
        condition.title = {
            [sequelize.Op.like]: '%' + title + '%'
        };
    // if(title)
    //     condition.push({title:{[sequelize.Op.like] : '%' + title + '%'}});
    if (filetype == 'mp4')
        condition.type = 'video/mp4';
    else if (filetype == 'pdf')
        condition.type = 'application/pdf';

    //sql 
    MerchantGuideline.findAll({
        where: condition,
        attributes: ['tutorial_id', 'title', 'type', 'tag', 'path'],
        order: [
            ['title', 'ASC'],

        ]

    }).then(list => {
        //console.log(list)
        return ReS(res, 'Successfully get data', {
            data: list
        }, 200);


    }).catch(err => {
        return ReE(res, err)
    })



}
module.exports.getGuidelineList = getGuidelineList;



